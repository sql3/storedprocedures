CREATE PROCEDURE [sp_FindInProjects] (@pSearch nvarchar(100))

BEGIN
  SET @pSearch = '%' + @pSearch + '%'

  SELECT CASE 
           WHEN SO.xtype = 'P' THEN 'Procedure' 
           WHEN SO.xtype = 'V' THEN 'View' 
		   WHEN SO.xtype = 'FN' THEN 'Function' 
           ELSE SO.xtype 
         END AS Typ, 
         SO.name, CONVERT(varchar(max), SM.definition ) AS Code, '' AS Code2
    FROM sys.sql_modules SM
   INNER JOIN sys.sysobjects SO ON SO.id = SM.object_id
   WHERE definition LIKE @pSearch
 
  ORDER BY 1,2
END
