CREATE PROCEDURE [dbo].[pFilePathExists]
(@filePath NVARCHAR(MAX), 
 @result   NVARCHAR(MAX) OUT
)
AS
     BEGIN

         -- required
         DECLARE @cmdShellResultTable TABLE
         (id       INT IDENTITY(1, 1), 
          [output] NVARCHAR(255) NULL
         );
         DECLARE @cmdShellResultCode INT;
         DECLARE @sqlString NVARCHAR(4000);
         DECLARE @parameterDeclare NVARCHAR(400);
         SET @sqlString = 'dir /A:-D '+@filePath+' /b';
         INSERT INTO @cmdShellResultTable(output)
         EXEC @cmdShellResultCode = xp_cmdshell 
              @sqlString;   -- master..

         IF @cmdShellResultCode = 0
             BEGIN
                 RETURN 0;
             END;

         -- Problem
         SELECT @result = 'ERROR '+isNull([output], '')
         FROM @cmdShellResultTable
         WHERE [output] IS NOT NULL
         ORDER BY id;
         RETURN;
         @cmdShellResultCode;
     END;
