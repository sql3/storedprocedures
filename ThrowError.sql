CREATE PROCEDURE ThrowError
AS
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
      @ErrorMessage = ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE();
    IF @ErrorState = 0 SET @ErrorState = 1   -- 0 

    SET @ErrText = 'ERROR Catched by <<PROCEDURE NAME>> / No: ' + ISNULL(CONVERT(nvarchar, ERROR_NUMBER()), '-') + ' / Sev:' + ISNULL(CONVERT(nvarchar, ERROR_SEVERITY()), '-') 
                   + ' /State:' + ISNULL(CONVERT(nvarchar, ERROR_STATE()), '-') 
                   + ' /Proc: ' + ISNULL(ERROR_PROCEDURE(), '-') 
                   + ' /Line: ' + ISNULL(CONVERT(nvarchar, ERROR_LINE()), '-') 
                   + ' /Msg: '  + ISNULL(ERROR_MESSAGE(), '-') 
                   + '/SQL: ' + @mSQL -- prevoiusly executed code with error
    INSERT INTO <<LOG TABLE>> () VALUES ()    ;

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage,   -- Message text.
                @ErrorSeverity,  -- Severity.
                @ErrorState      -- State.
              );
